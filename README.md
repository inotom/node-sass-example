# node-sass example

## development tasks

### install npm packages

```
$ make install
```

or

```
$ docker-compose run --rm npm install
```

### start development server

```
$ make start
```

or

```
$ docker-compose run --rm --publish 3000:3000 start
```

### build production dist

```
$ make build
```

or

```
$ docker-compose run --rm build
```

### linting

```
$ make lint
```

or

```
$ docker-compose run --rm lint
```

## volume

### list all volumes

```
$ docker volume ls
```

### remove volume

```
$ docker volume rm node-sass-example_dev_node_modules
```

## License

MIT

## Author

iNo (inotom)
