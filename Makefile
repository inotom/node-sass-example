CMD=docker-compose
ARG=sh

all: help

setup:
	go get github.com/Songmu/make2help/cmd/make2help

## install npm packages
install:
	$(CMD) run --rm npm install

## start development server
start:
	$(CMD) run --rm --publish 3000:3000 start

## build production dist
build:
	$(CMD) run --rm build

## linting
lint:
	$(CMD) run --rm lint

## stop container
stop:
	$(CMD) stop

## login container
login: run

## do some command (e.g. make run ARG='npm -v')
run:
	$(CMD) run --rm ${ARG}

## show command list
help:
	@make2help $(MAKEFILE_LIST)
